package com.blog.beans;

import com.blog.dao.CommentDao;
import com.blog.models.Comment;
import com.blog.models.Post;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Pavel
 * Date: 08.12.13
 * Time: 14:48
 * To change this template use File | Settings | File Templates.
 */

@ViewScoped
@ManagedBean(name = "commentBean")
public class CommentBean implements Serializable {

    @ManagedProperty("#{commentDao}")
    private CommentDao commentDao;

    @ManagedProperty("#{loginBean}")
    private LoginBean loginBean;

    private Comment comment;

    @PostConstruct
    public void init() {
        comment = new Comment();
    }

    public void save(long postId) {
        comment.setPostId(postId);
        comment.setAuthor(loginBean.getCurrentUser());
        commentDao.save(comment);
    }

    public CommentDao getCommentDao() {
        return commentDao;
    }

    public void setCommentDao(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }
}
