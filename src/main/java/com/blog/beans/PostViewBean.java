package com.blog.beans;

import com.blog.dao.CommentDao;
import com.blog.dao.PostDao;
import com.blog.models.Comment;
import com.blog.models.Post;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.view.ViewScoped;
import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@SessionScoped
@ManagedBean(name = "postViewBean")
public class PostViewBean implements Serializable {

    @ManagedProperty("#{loginBean}")
    private LoginBean loginBean;

    @ManagedProperty("#{postDao}")
    private PostDao postDao;

    @ManagedProperty("#{commentDao}")
    private CommentDao commentDao;

    private Post post;
    private Comment comment;
    private Long postId;


    @PostConstruct
    public void init() {
        comment = new Comment();
        post = new Post();
    }

    public void onIdInject(){
        post = postDao.get(postId);
    }

    public String savePost() {
        post.setAuthor(loginBean.getCurrentUser());
        post.setChangeDate(new Date());
        postDao.save(post);
        return "main";
    }

    public void saveComment() {
        comment.setPostId(postId);
        comment.setAuthor(loginBean.getCurrentUser());
        commentDao.save(comment);
        updateBean();
    }

    public void deleteComment(Long id) {
        commentDao.delete(id);
        updateBean();
    }

    private void updateBean() {
        post = postDao.get(postId);
        comment = new Comment();
    }


    //GS
    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public PostDao getPostDao() {
        return postDao;
    }

    public void setPostDao(PostDao postDao) {
        this.postDao = postDao;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public CommentDao getCommentDao() {
        return commentDao;
    }

    public void setCommentDao(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }
}
