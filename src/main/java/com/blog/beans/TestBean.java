package com.blog.beans;

import com.blog.dao.PostDao;
import com.blog.dao.UserDao;
import com.blog.models.Comment;
import com.blog.models.Post;
import com.blog.models.Role;
import com.blog.models.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.Date;

/**
 * User: Pavel
 * Date: 05.10.13
 * Time: 17:51
 */

@SessionScoped
@ManagedBean(name = "testBean")
public class TestBean {

    @ManagedProperty("#{userDao}")
    private UserDao userDao;

    @ManagedProperty("#{postDao}")
    private PostDao postDao;

    public String getTest() {
        return "0";
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setPostDao(PostDao postDao) {
        this.postDao = postDao;
    }
}
