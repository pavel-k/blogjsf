package com.blog.beans;

import com.blog.dao.PostDao;
import com.blog.models.Post;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.swing.text.View;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ViewScoped
@ManagedBean(name = "postListBean")
public class PostListBean implements Serializable {

    @ManagedProperty("#{postDao}")
    private PostDao postDao;

    private List<Post> postList = new ArrayList<Post>();
    private List<Long> paginationList = new ArrayList<Long>();
    private long limitPostOnPage = 5;
    private long currentPage = 0;
    private long size;

    @PostConstruct
    public void init() {

        //Init size
        size = postDao.getCount();

        //Init pagination list
        for (long i = 0; i < (float)size / (float)limitPostOnPage; i++) {
            paginationList.add(i);
        }
    }

    public void onIdInject() {
        long start = currentPage * limitPostOnPage;
        postList.addAll(postDao.getInRange(start, limitPostOnPage));
    }

    public List<Post> get() {
        return postList;
    }

    public List<Long> getPaginationList() {
        return paginationList;
    }



    //GS
    public PostDao getPostDao() {
        return postDao;
    }

    public void setPostDao(PostDao postDao) {
        this.postDao = postDao;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(long currentPage) {
        this.currentPage = currentPage;
    }
}
