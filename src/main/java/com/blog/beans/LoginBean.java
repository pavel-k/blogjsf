package com.blog.beans;

import com.blog.dao.UserDao;
import com.blog.models.Role;
import com.blog.models.User;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.security.*;
import java.security.MessageDigest;

@SessionScoped
@ManagedBean(name = "loginBean")
public class LoginBean implements Serializable {

    private User currentUser;
    private boolean login;

    @ManagedProperty("#{userDao}")
    private UserDao userDao;

    @ManagedProperty("#{testBean}")
    private TestBean testBean;

    @PostConstruct
    private void init() {
        if (this.currentUser == null) {
            currentUser = new User();
        }
    }

    public String login() {
        currentUser.setPasswordHash(md5(currentUser.getPasswordHash()));
        User user = userDao.getByName(currentUser.getName());

        if (user != null) {
            if (user.getPasswordHash().equals(currentUser.getPasswordHash())) {
                currentUser = user;
                login = true;
                return "main";
            }
        }

        FacesMessage msg = new FacesMessage("login error", "ERROR MSG");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return "login";
    }

    private String md5(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte byteData[] = md.digest();

            //convert the byte to hex format
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            return null;
        }

    }

    public boolean isAdmin(){
        return currentUser.getRole() == Role.Admin ? true : false;
    }

    public String logout() {
        login = false;
        return "main?faces-redirect=true";
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setTestBean(TestBean testBean) {
        this.testBean = testBean;
    }

    public boolean isLogin() {
        return login;
    }
}
