package com.blog.dao;

import com.blog.models.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CommentDao implements DAO<Comment> {

    @Autowired
    private UserDao userDao;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private RowMapper<Comment> commentRowMapper = new RowMapper<Comment>() {
        @Override
        public Comment mapRow(ResultSet rs, int rowNum) throws SQLException {
            Comment comment = new Comment();
            comment.setId(rs.getLong("ID"));
            comment.setBody(rs.getString("BODY"));
            comment.setAuthor(userDao.get(rs.getLong("AUTHOR_ID")));
            comment.setPostId(rs.getLong("POST_ID"));
            return comment;
        }
    };

    @Override
    public void save(Comment comment) {
        if(comment.getId() != null) {
            jdbcTemplate.update("UPDATE COMMENTS SET BODY=?, AUTHOR_ID=?, POST_ID=? WHERE id=?",
                    comment.getBody(), comment.getAuthor().getId(), comment.getPostId(), comment.getId());
        } else {
            jdbcTemplate.update("INSERT INTO COMMENTS(BODY, AUTHOR_ID, POST_ID) VALUES(?,?,?)",
                    comment.getBody(), comment.getAuthor().getId(), comment.getPostId());
        }
    }

    @Override
    public void delete(Comment comment) {
        delete(comment.getId());
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update("DELETE FROM COMMENTS WHERE id=?", id);
    }

    @Override
    public Comment get(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM COMMENTS WHERE id=?", new Object[] { id }, commentRowMapper);
    }

    @Override
    public List<Comment> getAll() {
        return jdbcTemplate.query("SELECT * FROM COMMENTS", commentRowMapper);
    }

    public List<Comment> getAllByPostId(Number id) {
        return jdbcTemplate.query("SELECT * FROM COMMENTS WHERE POST_ID=?", new Object[]{id}, commentRowMapper);
    }
}
