package com.blog.dao;

import com.blog.models.Comment;
import com.blog.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository("postDao")
public class PostDao implements DAO<Post> {

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private UserDao userDao;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private RowMapper<Post> rowMapper = new RowMapper<Post>() {
        @Override
        public Post mapRow(ResultSet rs, int rowNum) throws SQLException {
            Post post = new Post();
            post.setId(rs.getLong("ID"));
            post.setDescription(rs.getString("DESCRIPTION"));
            post.setBody(rs.getString("BODY"));
            post.setAuthor(userDao.get(rs.getLong("AUTHOR_ID")));
            post.setChangeDate(rs.getDate("DATE_CHANGE"));
            //List<Comment> comments = commentDao.getAllByPostId(post.getId());
            //post.getComments().addAll(comments);
            return post;
        }
    };

    private PreparedStatementCreator getPreparedStatementCreator(final Post post, final String sql) {
        return new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql);

                int i = 0;
                ps.setString(++i, post.getDescription());
                ps.setString(++i, post.getBody());
                ps.setDate(++i, new java.sql.Date(new Date().getTime()));
                ps.setLong(++i, post.getAuthor().getId());

                if (post.getId() != null) {
                    ps.setLong(++i, post.getId());
                }

                return ps;
            }
        };

    }

    @Override
    public void save(Post post) {
        if (post.getId() != null) {
            jdbcTemplate.update("UPDATE POSTS SET DESCRIPTION=?, BODY=?, DATE_CHANGE=? , AUTHOR_ID=? WHERE id=?",
                    post.getDescription(), post.getBody(), post.getChangeDate(), post.getAuthor(), post.getId());

            for (Comment comment : post.getComments()) {
                commentDao.save(comment);
            }
        } else {
            KeyHolder holder = new GeneratedKeyHolder();
            PreparedStatementCreator ps = getPreparedStatementCreator(post, "INSERT INTO POSTS(DESCRIPTION, BODY, DATE_CHANGE, AUTHOR_ID) VALUES(?,?,?,?)");
            jdbcTemplate.update(ps, holder);

            for (Comment comment : post.getComments()) {
                comment.setPostId(holder.getKey().longValue());
                commentDao.save(comment);
            }
        }
    }

    @Override
    public void delete(Post post) {
        for (Comment comment : post.getComments()) {
            commentDao.delete(comment);
        }
        jdbcTemplate.update("DELETE FROM POSTS WHERE id=?", post.getId());
    }

    @Override
    public void delete(Long id) {
        Post post = get(id);
        for (Comment comment : post.getComments()) {
            commentDao.delete(comment);
        }
        delete(post);
    }

    @Override
    public Post get(Long id) {
        Post post = jdbcTemplate.queryForObject("SELECT * FROM POSTS WHERE id=?", new Object[]{id}, rowMapper);
        post.setComments(commentDao.getAllByPostId(post.getId()));
        return post;
    }

    @Override
    public List<Post> getAll() {
        List<Post> posts = jdbcTemplate.query("SELECT * FROM POSTS", rowMapper);
        for (Post post : posts) {
            post.setComments(commentDao.getAllByPostId(post.getId()));
        }
        return posts;
    }

    public List<Post> getInRange(long start, long limit) {
        List<Post> posts = jdbcTemplate.query("SELECT * FROM POSTS LIMIT ? OFFSET ?", new Object[]{limit, start}, rowMapper);
        return posts;
    }

    public int getCount() throws DataAccessException {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM POSTS",Integer.class);
    }
}
