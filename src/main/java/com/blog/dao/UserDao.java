package com.blog.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import com.blog.models.Role;
import com.blog.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository("userDao")
public class UserDao implements DAO<User> {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private RowMapper<User> rowMapper = new RowMapper<User>() {
        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setId(rs.getLong("ID"));
            user.setName(rs.getString("NAME"));
            user.setPasswordHash(rs.getString("PASSWORD_HASH"));
            user.setRole(Role.User);
            return user;
        }
    };

    @Override
    public void save(User user) {
        if(user.getId() != null) {
            jdbcTemplate.update("UPDATE USERS SET NAME=?, PASSWORD_HASH=?, ROLE=? WHERE id=?",
                    user.getName(), user.getPasswordHash(), user.getRole().ordinal(), user.getId());
        }

        else {
            jdbcTemplate.update("INSERT INTO USERS (NAME, PASSWORD_HASH, ROLE) VALUES(?,?,?)",
                    user.getName(), user.getPasswordHash(), user.getRole().ordinal());
        }
    }

    @Override
    public void delete(User user) {
        delete(user.getId());
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update("DELETE FROM USERS WHERE id=?", id);
    }

    @Override
    public User get(Long id) {
        return jdbcTemplate.queryForObject("SELECT * FROM USERS WHERE id=?", new Object[] { id }, rowMapper);
    }

    @Override
    public List<User> getAll() {
        return jdbcTemplate.query("SELECT * FROM USERS", rowMapper);
    }

    public User getByName(String name) {
        User user = null;
        List<User> users = jdbcTemplate.query("SELECT * FROM USERS WHERE NAME=?", new Object[] { name }, rowMapper);
        if(users.size()>0) {
            user = users.get(0);
        }

        return user;
    }
}
